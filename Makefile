all: game.dcl.html LetsExploreGeography-dist.zip

game.dcl.html: cancomtrad.mdcl data/city_data.mdcl data/commodity_data.mdcl data/tourism_data.mdcl
	macrodcl $^ > $@

data/cgn_canada_csv_eng.zip:
	echo "Downloading Canadian place name database..."
	mkdir -p data
	curl http://ftp.geogratis.gc.ca/pub/nrcan_rncan/vector/geobase_cgn_toponyme/prov_csv_eng/cgn_canada_csv_eng.zip -o $@
data/cgn_canada_csv_eng.csv: data/cgn_canada_csv_eng.zip
	unzip -d data -o $^
	touch $@
data/city_data.mdcl data/important_cities.txt data/city_data.geojson: make_city_data.py data/cgn_canada_csv_eng.csv
	./make_city_data.py
data/commodity_data.mdcl LetsExploreGeography-dist/walkthrough.html: make_commodities_data.py data/important_cities.txt walkthrough_static_part.html
	./make_commodities_data.py
data/tourism_data.mdcl: make_tourism_data.py tourism.csv data/cgn_canada_csv_eng.zip
	./make_tourism_data.py

LetsExploreGeography-dist.zip: LetsExploreGeography-dist/index.html LetsExploreGeography-dist/map.pdf LetsExploreGeography-dist/walkthrough.html LetsExploreGeography-dist/game.js LetsExploreGeography-dist/game.html
	zip $@ $^

data/game_function.js: cancomtrad.mdcl data/city_data.mdcl data/commodity_data.mdcl data/tourism_data.mdcl
	macrodcl-js $^ > $@

LetsExploreGeography-dist/game.js: data/game_function.js game_scaffold.js
	cat $^ | closure-compiler --compilation_level ADVANCED --js_output_file $@
