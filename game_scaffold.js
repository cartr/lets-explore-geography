// Misc functions called by MacroDCL

function func$not(val) {
    return !val;
}

function func$numericValue(val) {
    // Shim implementations of the only two operations we use
    if (val.startsWith("\\floor(")) {
        return Math.floor(val.substring(7, val.length-1));
    } else if (val.startsWith("\\operatorname{mod}(")) {
        var args = val.substring(19, val.length-1).split(",");
        return args[0] % args[1];
    } else {
	console.log("WARNING: could not parse " + val + ", returning NaN");
	return NaN;
    }
}

// The Action Button, which stores all the game state

var press_count = 0;
var press_time = new Date().getTime() / 1000;
function source$button$pressCount() {
    return press_count;
}

var current_capture_vars = {};
var next_capture_vars = {};
function sink$button$capture(variable, value) {
    next_capture_vars[variable] = value;
}
function source$button$lastValue(variable) {
    return current_capture_vars[variable];
}
function button_pushed() {
    current_capture_vars = next_capture_vars;
    window.localStorage.setItem("leg-captures", JSON.stringify(current_capture_vars));
    next_capture_vars = {};
    press_count += 1;
    press_time = new Date().getTime() / 1000;
    update_mdcl_state();
}


function sink$button$hidden(hidden) {
    document.getElementById("button").style.display = hidden ? "none" : "block";
}

var timeSincePressTimeout = undefined;
function source$button$timeSincePress(timer_length) {
    var timedelta = new Date().getTime() / 1000 - press_time;
    if (timedelta > timer_length) {
        return timer_length;
    } else {
	if (timeSincePressTimeout === undefined) {
	    timeSincePressTimeout = setTimeout(function() {
                timeSincePressTimeout = undefined;
		update_mdcl_state();
            }, 50);
        }
        return timedelta;
    }
}


// The text sinks, used to give feedback to the user.

function sink$description$content(content) {
    document.getElementById("description").textContent = content;
}

function sink$choices$choiceContent(radio, content) {
    if (content === "") {
        document.getElementById("radio" + radio + "-wrapper").style.display = "none";
        document.getElementById("button" + radio).style.display = "none";
    } else {
        document.getElementById("radio" + radio + "-wrapper").style.display = "block";
        document.getElementById("radio" + radio + "-label").textContent = content;
        document.getElementById("button" + radio).textContent = content;
        document.getElementById("button" + radio).style.display = "block";
    }
}

function sink$inventory_screen$content(content) {
    document.getElementById("inventory_screen").textContent = content;
}


// The Choices, which the user uses to choose stuff

function source$choices$isSelected(radio) {
    return document.getElementById("radio"+radio).checked;
}

function make_button_listener(radio) {
    return function () {
        // If a user clicks a "quick choice" button, just clicking the
        // corresponding radio button and pushing Submit.
        document.getElementById("radio" + radio).checked = true;
        update_mdcl_state();
        button_pushed();
    };
}

for (var i = 1; i <= 6; ++i) {
    document.getElementById("radio" + i).addEventListener("change", update_mdcl_state);
    document.getElementById("button" + i).addEventListener("click", make_button_listener(i));
}


document.getElementById("button").addEventListener("click", button_pushed);

document.getElementById("next-page").addEventListener("click", function() {
    document.body.className = "onpage2";
    document.getElementById("prev-page").disabled = false;
    document.getElementById("page-span").textContent = "2 of 2";
    document.getElementById("next-page").disabled = true;
});
document.getElementById("prev-page").addEventListener("click", function() {
    document.body.className = "onpage1";
    document.getElementById("prev-page").disabled = true;
    document.getElementById("page-span").textContent = "1 of 2";
    document.getElementById("next-page").disabled = false;
});

document.getElementById("main").addEventListener("click", function() {
    document.getElementById("settings-window").style.display = "none";
});
document.getElementById("settings").addEventListener("click", function() {
    document.getElementById("settings-window").style.display = "block";
});

var previousCaptures = window.localStorage.getItem("leg-captures");
if (previousCaptures !== null) {
    current_capture_vars = JSON.parse(previousCaptures);
    previousCaptures = null;
    console.log(current_capture_vars);
    press_count = 1
}


function pick_classic_choices() {
    document.getElementById("main").className = "classicchoices";
    document.getElementById("settings-classic").className = "selected";
    document.getElementById("settings-button").className = "";
    window.localStorage["leg-choice"] = "classic";
}

function pick_button_choices() {
    document.getElementById("main").className = "buttonchoices";
    document.getElementById("settings-classic").className = "";
    document.getElementById("settings-button").className = "selected";
    window.localStorage["leg-choice"] = "button";
}

if (window.localStorage["leg-choice"] == "button") {
    pick_button_choices();
}
document.getElementById("settings-classic").addEventListener("click", pick_classic_choices);
document.getElementById("settings-button").addEventListener("click", pick_button_choices);

function pick_classic_inventory() {
    document.body.className="onpage1";
    document.getElementById("inventory-classic").className = "selected";
    document.getElementById("inventory-sbs").className = "";
    window.localStorage["leg-inventory"] = "classic";
}
function pick_sbs_inventory() {
    document.body.className="sbsinventory";
    document.getElementById("inventory-classic").className = "";
    document.getElementById("inventory-sbs").className = "selected";
    window.localStorage["leg-inventory"] = "sbs";
}
if (window.localStorage["leg-inventory"] == "sbs") {
    pick_sbs_inventory();
}
document.getElementById("inventory-classic").addEventListener("click", pick_classic_inventory);
document.getElementById("inventory-sbs").addEventListener("click", pick_sbs_inventory);


document.getElementById("reset").addEventListener("click", function() {
    if (window.confirm("Are you sure? You'll lose all of your progress!")) {
        current_capture_vars = {};
        press_count = 0;
        window.localStorage.removeItem("leg-captures");
        update_mdcl_state();
    }
});

update_mdcl_state();
