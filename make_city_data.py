#!/usr/bin/env python3

roads = (
    # Newfoundland
    ('southeast', 'ABEFS', 'AAHSV', 'northwest'),
    ('east', 'AAHSV', 'AAEOU', 'west'),
    ('northeast', 'AAEOU', 'AADVG', 'southwest'),
    # Newfoundland <-> Nova Scotia
    ('ferry', 'AADVG', 'CBLGX', 'ferry'),
    # Nova Scotia
    ('northeast', 'CBLGX', 'CAPHL', 'southwest'),
    ('northeast', 'CAPHL', 'CBPIB', 'southwest'),
    # Nova Scotia <-> New Brunswick
    ('southeast', 'CAPHL', 'DADHJ', 'northwest'),
    # New Brunswick <-> PEI
    ('west', 'DADHJ', 'BAARG', 'east'),
    # New Brunswick
    ('east', 'DADHJ', 'DAFMJ', 'west'),
    ('south', 'DADHJ', 'DAFQX', 'north'),
    ('southwest', 'DAFMJ', 'DAFQX', 'northeast'),
    ('south', 'DAEGW', 'DAFMJ', 'north'),
    # New Brunswick <-> Quebec
    ('east', 'DAFMJ', 'nb_qc_border', 'EQRRW', 'EHTWR', 'west'),
    # Quebec
    ('south', 'EHTWR', 'ERBYN', 'north'),
    ('east', 'EHTWR', 'EPUWF', 'EHHUN', 'west'),
    ('southeast', 'EHHUN', 'EKXJO', 'EJBUU', 'EIJZE', 'northwest'),
    # Quebec <-> Labrador
    ('south', 'ERBYN', 'EGWAO', 'EJCRF', 'EPIHT', 'ABFQA', 'north'),
    # Quebec <-> Ontario
    ('east', 'EHHUN', 'FEVNT', 'west'),
    ('east', 'EIJZE', 'qc_on_border', 'FCWMG', 'west'),
    # Ontario
    ('north', 'FCWMG', 'FAZRZ', 'FCTOV', 'south'),
    ('east', 'FEVNT', 'FCKPW', 'FDNRQ', 'FCTOV', 'west'),
    ('north', 'FCTOV', 'FDSUS', 'FAFFD', 'FEUZB', 'south'),
    ('east', 'FEUZB', 'FCAEN', 'FDEGT', 'west'),
    ('east', 'FCTOV', 'FAVBI', 'FDZCP', 'west'),
    ('east', 'FDZCP', 'FDGDZ', 'FDNRF', 'lake_superior', 'FCWFX', 'west'),
    # Ontario <-> Manitoba
    ('east', 'FCWFX', 'FDZXQ', 'FBPXK', 'FAZWK', 'on_mn_border', 'GBEIN', 'west'),
    # Manitoba
    ('south', 'GBEIN', 'GAHUP', 'GAKCE', 'GAJQX', 'north'),
    ('south', 'GAJQX', 'GBINH', 'GAOEP', 'GBBRP', 'north'),
    ('southeast', 'GAJQX', 'GBEAK', 'GAILW', 'northwest'),
    ('east', 'GBEIN', 'GABIG', 'GADMZ', 'west'),
    # Manitoba <-> Saskatchewan
    ('east', 'GADMZ', 'mn_sk_border1', 'HAIMP', 'west'),
    ('southeast', 'GADMZ', 'mn_sk_border2', 'HAVWM', 'northwest'),
    # Saskatchewan
    ('northeast', 'HAVWM', 'HAIMP', 'southwest'),
    ('east', 'HAVWM', 'HACBK', 'HATNJ', 'HAHJJ', 'west'),
    ('south', 'HAHJJ', 'HASUT', 'HALRP', 'HAFJY', 'north'),
    ('east', 'HAIMP', 'HALTS', 'HAPKT', 'west'),
    ('southeast', 'HAHJJ', 'HACRM', 'HAGJP', 'northwest'),
    # (note: HAGJP is special border town)
    # Saskatchewan <-> Alberta
    ('east', 'HAGJP', 'IAMFL', 'IACMP', 'west'),
    ('east', 'HAHJJ', 'HAERH', 'sk_ab_border', 'IASEQ', 'IABPA', 'IAKID', 'west'),
    ('east', 'HAPKT', 'sk_ab_border', 'IAIUC', 'IADGP', 'west'),
    # Alberta
    ('south', 'IADGP', 'IAKID', 'north'),
    ('east', 'IAKID', 'IACYE', 'west'),
    ('south', 'IAKID', 'IAEJS', 'north'),
    ('south', 'IAEJS', 'IACMP', 'north'),
    ('southeast', 'IACMP', 'IAPCL', 'IADUJ', 'IAHMM', 'northwest'),
    ('south', 'IAHMM', 'IAEWL', 'IAEWG', 'IAJVJ', 'north'),
    # Alberta <-> Northwest Territories
    ('south', 'IAJVJ', 'ab_nt_border', 'LAKGC', 'LBAMG', 'north'),
    # Alberta <-> British Columbia
    ('southeast', 'IAHMM', 'JAPCN', 'JAGPB', 'northwest'),
    ('east', 'IACMP', 'IACMW', 'IAJWU', 'ab_bc_border', 'JAMKO', 'JBLVS', 'west'),
    ('east', 'IACYE', 'JBZLB', 'JCJTL', 'JAFNW', 'west'),
    ('east', 'IADGP', 'ab_bc_border', 'JAIQY', 'west'),
    # British Columbia
    ('east', 'JAIQY', 'JCAAC', 'bc_junction_1', 'west'),
    ('east', 'bc_junction_1', 'JAINK', 'JCVCB', 'JBRIK', 'west'),
    ('ferry', 'JBRIK', 'JBOBQ', 'ferry'),
    ('south', 'bc_junction_1', 'JAFUV', 'north'),
    ('southwest', 'JBRIK', 'JBJLV', 'JAFNW', 'northeast'),
    ('south', 'JBRIK', 'JCJHE', 'JCJOA', 'JBTMB', 'north'),
    ('southeast', 'JAFNW', 'JCCIT', 'JBTMB', 'northwest'),
    ('south', 'JBTMB', 'JBNTS', 'JBLVS', 'north'),
    ('south', 'JBLVS', 'JBSWI', 'JDESR', 'JAGPB', 'north'),
    ('east', 'bc_junction_2', 'JCNWW', 'west'),
    ('south', 'JAFUV', 'JAFNW', 'north'),
    ('south', 'JBLVS', 'JAIOO', 'JBCZQ', 'JBMQO', 'bc_junction_2', 'northeast'),
    # British Columbia <-> Yukon
    ('south', 'bc_junction_2', 'JAACM', 'JDCYU', 'JCUIV', 'JBDCY', 'KAHDJ', 'northeast'),
    ('southeast', 'JAGPB', 'JCTRO', 'JDIGT', 'JCLIN', 'JAWVA', 'KAHDJ', 'northwest'),
    # Yukon
    ('east', 'KAHDJ', 'KAGLF', 'KADFC', 'KAHFT', 'west'),
    ('south', 'KAHFT', 'KAEIU', 'KACGA', 'KABPC', 'north'),
    # Yukon <-> Northwest Territories
    ('south', 'KABPC', 'yk_empty', 'KABWA', 'yk_nt_border', 'LALNA', 'north')
)

betweens = {"bc_junction_1": ("JCAAC", "JAINK"), "bc_junction_2": ("JBMQO", "JAACM")}

sceneries = {
    'nb_qc_border': ('You see a sign that says, "Bienvenue au Québec !"', 'You see a sign that reads, "Welcome to New Brunswick."'),
    'qc_on_border': ('You see a sign that says, "Welcome to Ontario!"', 'You see a sign that reads, "Bienvenue au Québec !" Below it, in much smaller letters, is a sign that reads "Welcome to Quebec!"'),
    'on_mn_border': ('You see a sign that says:\n\nBienvenue au\nMANITOBA\nwelcomes you', 'Welcome back to Ontario!'),
    'mn_sk_border1': ('You see a sign that says, "Saskatchewan, naturally." You suppose that\'s one way to welcome someone to your province.', '"Bienvenue au\nMANITOBA\nwelcomes you'),
    'mn_sk_border2': ('You see a sign that says, "Saskatchewan, naturally." You suppose that\'s one way to welcome someone to your province.', '"Bienvenue au\nMANITOBA\nwelcomes you"'),
    'sk_ab_border': ('A sign welcomes you to Alberta.', '"Saskatchewan, naturally."'),
    'ab_nt_border': ('A sign says, "60th Parallel - Welcome to the Northwest Territories."', 'You drive back across the border to Alberta.'),
    'ab_bc_border': ('You see a sign welcoming you to British Columbia - "The Best Place on Earth!"', 'A sign welcomes you to Alberta.'),
    'yk_nt_border': ('You see a sign that says, "Welcome to spectacular Northwest Territories."', 'You see a sign that says, "WELCOME TO YUKON".'),

    'lake_superior': ('You look to your left and catch a great view of Lake Superior. You\'d look at it longer, but you need to keep your eyes on the road.', 'You pass Lake Superior.'),
    'yk_empty': ('...', '...')
}

# ========== end data ==========

import csv
import unicodedata

outfile = open('data/city_data.mdcl', 'w')

def slugify(place_name):
    result = ""
    for char in unicodedata.normalize('NFKD', place_name):
        if char.isalnum():
            result += char.lower()
    return result

places_needed = set()
important_places = set()
for road in roads:
    places_needed.update(road[1:-1])
    important_places.add(road[1])
    important_places.add(road[-2])

junctions = {}
for place in important_places:
    if 'junction' in place:
        junctions[place] = []

places = {}

with open('data/cgn_canada_csv_eng.csv', encoding='utf-8') as f:
    reader = iter(csv.reader(f))
    labels = next(reader)
    labels[0] = labels[0].strip("\ufeff")
    for row in reader:
        if row[0] in places_needed:
            place_object = {}
            for i, label in enumerate(labels):
                place_object[label] = row[i]
            place_object["Slug"] = slugify(place_object["Geographical Name"])
            if place_object["Geographical Name"] == "Québec":
                place_object["Geographical Name"] = "Québec City"
            elif place_object["Geographical Name"] == "Lloydminster":
                place_object["Province - Territory"] = "Alberta/Saskatchewan"
            places[row[0]] = place_object

city_departing_navnodes = {}
for place in places:
    city_departing_navnodes[place] = []

roads_with_reverses = []
for road in roads:
    roads_with_reverses.append((0, road))
    roads_with_reverses.append((1, tuple(reversed(road))))
for (scenery_index, road) in roads_with_reverses:
    depart_direction = road[-1]
    cities = road[1:-1]
    city_slugs = tuple(map(lambda name: places[name]["Slug"] if name in places else name, cities))
    destination_slug = city_slugs[-1] + "_from_" + city_slugs[0]
    if 'junction' in cities[0]:
        junctions[cities[0]].append((depart_direction, cities[-1]))
    for i, city in enumerate(cities[:-1]):
        navnode_slug = city_slugs[i] + "_to_" + city_slugs[-1]
        print("navnode ["+navnode_slug+"] {", file=outfile)
        if city in places:
            print("    current_city = city_" + city_slugs[i], file=outfile)
            if cities[-1] in places:
                city_departing_navnodes[city].append((depart_direction.title() + " to " + places[cities[-1]]["Geographical Name"], city_slugs[-1]))
            elif cities[-1] in betweens and cities[-2] in betweens[cities[-1]]:
                for between in betweens[cities[-1]]:
                    if between != cities[-2]:
                        for _, between_road in roads_with_reverses:
                            if between_road[1] == cities[-1] and between_road[2] == between:
                                city_departing_navnodes[city].append((depart_direction.title() + ", towards " + places[between_road[-2]]["Geographical Name"], city_slugs[-1]))
            else:
                city_departing_navnodes[city].append(("Head " + depart_direction, city_slugs[-1]))
        elif city in sceneries:
            print("    current_navnode_can_pull_over = false", file=outfile)
            scenery_text = sceneries[city][scenery_index]
            print("    current_navnode_scenery_text = \"" + scenery_text.replace('"', '\\"') + "\"", file=outfile)
        else:
            print("    current_navnode_can_pull_over = false", file=outfile)
        if i == 0:
            print("    current_navnode_is_start = true", file=outfile)
            if cities[-1] in places:
                print("    current_navnode_destination_city = \"" + places[cities[-1]]["Geographical Name"]+"\"", file=outfile)
            else:
                print("    current_navnode_scenery_text = \"You head " + depart_direction +".\"", file=outfile)
        print("}\n", file=outfile)
    print("navnode ["+destination_slug+"] {", file=outfile)
    if cities[-1] in places:
        print("    current_navnode_is_destination = true", file=outfile)
        print("    current_city = city_" + city_slugs[-1], file=outfile)
    elif "junction" in cities[-1]:
        print("    current_navnode_scenery_text = \"You reach a fork in the road. Which way would you like to go?\"", file=outfile)
        print("    current_navnode_can_continue = false", file=outfile)
        print("    current_navnode_can_pull_over = false", file=outfile)
        # Note: junction choices set below, in when-block
    print("}\n", file=outfile)

for place in sorted(places):
    slug = places[place]["Slug"]
    print("city ["+slug+"] {", file=outfile)
    print("    current_city_name = \"" + places[place]["Geographical Name"] + "\"", file=outfile)
    print("    current_city_province = \"" + places[place]["Province - Territory"] + "\"", file=outfile)
    print("    current_city_latitude = " + places[place]["Latitude"], file=outfile)
    print("    current_city_longitude = " + places[place]["Longitude"], file=outfile)
    print("    current_city_cgndb_code = \"" + places[place]["CGNDB ID"] + "\"", file=outfile)
    if place in important_places:
        print("    current_city_is_big = true", file=outfile)
    print("    when current_page = page_drivemenu {", file=outfile)
    for i, (text, destination) in enumerate(city_departing_navnodes[place]):
        print("        choice ["+str(i+2)+", \"" + text + "\"] {", file=outfile)
        print("            next_page = page_driving", file=outfile)
        print("            next_navnode = navnode_" + slug + "_to_" + destination, file=outfile)
        print("        }", file=outfile)
    print("    }", file=outfile)
    print("}\n", file=outfile)

for junction in junctions:
    for i, (_, from_city) in enumerate(junctions[junction]):
        print("when current_page = page_driving and current_navnode = navnode_"+junction+"_from_"+places[from_city]["Slug"]+" {", file=outfile)
        choice_number = 1
        for j, (direction, to_city) in enumerate(junctions[junction]):
            if i == j:
                continue
            print("    choice ["+str(choice_number)+", \""+direction.title()+", towards "+places[to_city]["Geographical Name"]+"\"]  {", file=outfile)
            print("        next_navnode = navnode_" + junction + "_to_" + places[to_city]["Slug"], file=outfile)
            print("    }", file=outfile)
            choice_number += 1

        print("}\n", file=outfile)

with open('data/important_cities.txt', 'w') as cityfile:
    for place in important_places:
        if place in places:
            print(places[place]["Slug"] + "\t" + places[place]["Geographical Name"], file=cityfile)

import json
place_coordinates = {}
geojson_features = []
geojson = {"type": "FeatureCollection", "features": geojson_features}
for place in places_needed:
    if place in places:
        place_coordinates[place] = (float(places[place]["Longitude"]), float(places[place]["Latitude"]))
        place_feature = {"type": "Feature", "geometry": {
            "type": "Point",
            "coordinates": place_coordinates[place]
        }, "properties": {
            "is_big_city": "yes" if place in important_places else "no",
            "name": places[place]["Geographical Name"]
        }}
        geojson_features.append(place_feature)
    elif place in betweens:
        bt1 = places[betweens[place][0]]
        bt2 = places[betweens[place][1]]
        place_coordinates[place] = (
                (float(bt1["Longitude"]) + float(bt2["Longitude"])) / 2,
                (float(bt1["Latitude"]) + float(bt2["Latitude"])) / 2,
        )
for road in roads:
    road_coords = []
    for place in road[1:-1]:
        if place in place_coordinates:
            road_coords.append(place_coordinates[place])
        else:
            print("WARNING:", place, "is missing coordinates")
    geojson_features.append({
        "type": "Feature",
        "geometry": {
            "type": "LineString",
            "coordinates": road_coords,
        }})

with open('data/city_data.geojson', 'w') as geojson_file:
    json.dump(geojson, geojson_file)
