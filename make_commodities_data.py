#!/usr/bin/env python3

city_commodities = {
    "saintjohn": 'tissues', # https://en.wikipedia.org/wiki/Saint_John,_New_Brunswick#Economy mentions a tissue paper plant
    "thunderbay": 'amethysts', # We went to the amethyst
    "kelowna": 'raisins', # https://en.wikipedia.org/wiki/Kelowna#Economy mentions wineries, but this is a kids' game
    "lloydminster": "oil", # https://en.wikipedia.org/wiki/Lloydminster#Economy
    "grandrapids": "walleye", # A type of fish Google says you can catch in Grand Rapids, Manitoba
    "banff": "shirts", # Banff is a resort town, so technically these are souvenir t-shirts.
    "sudbury": "nickel", # https://en.wikipedia.org/wiki/Greater_Sudbury
    "inuvik": "pike", # https://spectacularnwt.com/what-to-do/fishing/species
    "grandeprairie": "grain", # https://en.wikipedia.org/wiki/Grande_Prairie#Economy
    "laronge": "rice", # https://en.wikipedia.org/wiki/La_Ronge mentions La Ronge Rice Company
    "toronto": "snowglobes", # Another silly souvenir commodity
    "timmins": "zinc", # https://en.wikipedia.org/wiki/Timmins
    "stjohns": "oil", # https://en.wikipedia.org/wiki/St._John's%2C_Newfoundland_and_Labrador#Economy
    "yellowknife": "diamonds", # https://en.wikipedia.org/wiki/Yellowknife
    "moncton": "grass", # https://business.financialpost.com/commodities/agriculture/organigram-announces-60-million-pot-deal-with-new-brunswick , but this is a kids' game
    "edmonton": "oil", # https://en.wikipedia.org/wiki/Edmonton mentions "Oil Capital of Canada" in the 40s
    "reddeer": "grain", # https://en.wikipedia.org/wiki/Red_Deer%2C_Alberta
    "fredericton": "antennas", # https://en.wikipedia.org/wiki/Fredericton#Economy mentions IT, free wifi???
    "halifax": "limestone", # https://en.wikipedia.org/wiki/Halifax%2C_Nova_Scotia#Economy
    "labradorcity": "iron", # https://en.wikipedia.org/wiki/Labrador_City#Economy
    "montreal": "videogames", # Ubisoft is based here I think
    "kamloops": "copper", # https://en.wikipedia.org/wiki/Kamloops#Industry
    "watsonlake": 'lumber', # https://en.wikipedia.org/wiki/Watson_Lake%2C_Yukon
    "whitehorse": "grain", # http://www.yukonfromthegroundup.ca/yukongrainfarm.php
    "valdor": "gold", # I speak French
    "victoria": "lumber", # https://prezi.com/a1qhbeiezjxy/the-industries-of-victoria-bc/
    "quebec": "paper", # https://en.wikipedia.org/wiki/Quebec_City#Economy
    "bathurst": "zinc", # https://en.wikipedia.org/wiki/Bathurst_Mining_Camp
    "cranbrook": "coal", # http://cranbrook.ca/our-city/information-and-statistics/economic-base/
    "sydney": "steel", # https://en.wikipedia.org/wiki/Sydney,_Nova_Scotia
    "yarmouth": "lobster", # https://en.wikipedia.org/wiki/Yarmouth%2C_Nova_Scotia
    "gander": "magnets", # ??? I dunno, this city doesn't seem to export anything
    "saskatoon": "saskatoons", # it's a type of berry, look it up
    "ottawa": "telephones", # Alcatel, BlackBerry, etc.
    "vancouver": "coal", # http://www.vancouversun.com/technology/Vancouver+coal+ports+spotlight/8594760/story.html
    "williamslake": "lumber", # https://en.wikipedia.org/wiki/Williams_Lake%2C_British_Columbia#Economy
    "regina": "potash", # https://en.wikipedia.org/wiki/Regina%2C_Saskatchewan#Economy suprisingly not drug-related
    "saultstemarie": "steel", # https://en.wikipedia.org/wiki/Sault_Ste._Marie%2C_Ontario#Economy
    "lethbridge": "birdseed", # https://en.wikipedia.org/wiki/Economy_of_Lethbridge#Manufacturing mentions "animal feed", this is close enough
    "thompson": "nickel", # https://en.wikipedia.org/wiki/Thompson,_Manitoba#Economy
    "flinflon": "zinc", # https://en.wikipedia.org/wiki/Flin_Flon#Mining
    "dawson": "gold", # https://en.wikipedia.org/wiki/Dawson_City
    "swiftcurrent": "leather", # http://www.swiftcurrent.ca/divisions/planning-growth-development/business-development mentions cattle
    "brandon": "grain", # https://en.wikipedia.org/wiki/Brandon%2C_Manitoba
    "saguenay": "aluminum", # https://en.wikipedia.org/wiki/Saguenay%2C_Quebec#Economy
    "calgary": "oil", # https://en.wikipedia.org/wiki/Calgary#Economy
    "cornerbrook": "paper", # https://www.cbc.ca/news/canada/newfoundland-labrador/pulp-paper-tariffs-ruling-1.4803197
    "windsor": "engines", # https://en.wikipedia.org/wiki/Windsor%2C_Ontario
    "highlevel": "flakeboard", # https://en.wikipedia.org/wiki/High_Level#Economy
    "fortstjohn": "flakeboard", # https://en.wikipedia.org/wiki/Fort_St._John%2C_British_Columbia
    "princegeorge": "sulphur", # www.sulphuric-acid.com/sulphuric-acid-on-the-web/Acid Plants/Marsulex - Prince George.htm (but "sulphuric acid" would be two words)
    "charlottetown": "wigs", # a typical PEI souvenir thanks to Anne of Green Gables
    "channelportauxbasques": "mackerel", # https://www.newfoundlandlabrador.com/plan-and-book/travel-offers/1648
    "winnipeg": "loonies", # Canadian one-dollar coins are made in Winnipeg - https://en.wikipedia.org/wiki/Loonie
    "yorkton": "canola", # https://www.yorkton.ca/dept/econdev/highlights.asp
    "princerupert": "salmon", # https://visitprincerupert.com/see-and-do/fishing/
}

# Pallet notes: Standard pallet loads are 48x40x70in (=2200 L) according to https://pe.usps.com/qsg_archive/PDF/QSG_Archive_20060108/qsg300/q705b.pdf
# Max weight of a pallet is 4600 lbs (=2000 kg) according to https://www.freightquote.com/how-to-ship-freight/standard-pallet-sizes

commodity_pallet_prices = {
    # You can totally just type "price of 2000 kg of nickelin CAD" into WolframAlpha to get prices
    "nickel": 59710,
    "steel": 2486,
    "zinc": 6104,
    "iron": 77,
    "aluminum": 6417,
    "copper": 23350,
    "amethysts": 67000, # ??? completely fake number
    "gold": 200000, # 2000 kg of gold is $77 million, which is crazy, so I just made up a number. (Maybe it's packed in really bulky containers or something.)
    "diamonds": 30000000, # https://www.quora.com/How-much-would-1-kg-of-2-carat-diamond-cost I used "natural colorless" price
    "potash": 570, # http://www.infomine.com/investment/metal-prices/potash/ gives $226 USD per ton
    "flakeboard": 1133, # https://www.homedepot.com/b/Lumber-Composites-Plywood-Oriented-Strand-Board-OSB/N-5yc1vZbqpq gives 13.05 USD per (7/16 in * 48 in * 8 ft)
    "rice": 1102, # https://www.indexmundi.com/commodities/?commodity=rice gives $445 USD per ton
    "walleye": 115900, # https://www.marxfoods.com/Bulk-Walleye-Fillets gives $23.7 USD per pound, http://www.fao.org/wairdocs/tan/x5898e/x5898e01.htm says fish are ~50 pounds/m3
    "salmon": 145400, # https://www.alaskagoldbrand.com/product/king-salmon-bulk-orders-portions/ gives $595 USD for 20 pounds
    "raisins": 1540,  # Raisins cost like a dollar a pound, and density when packed is 0.7 kg/L
    "telephones": 31080, # "50 dollars * 2200 L / 6 inches / 6 inches / 6 inches"
    "paper": 1630, # https://www.amazon.com/Relay-Printer-Multipurpose-Letter-Bright/dp/B01LXLDBBO/ref=sr_1_3?ie=UTF8&qid=1535774767&sr=8-3&keywords=pallet+of+paper
    "tissues": 800, # https://www.amazon.com/Scott-Toilet-Paper-Sheets-Pallet/dp/B07FKF893T/ref=sr_1_6_a_it?ie=UTF8&qid=1535774847&sr=8-6&keywords=pallet+of+tissues . Tissues are basically toilet paper, right?
    "snowglobes": 20980, # Assume $10 souvenir snowglobes, four inches cubed
    "sulphur": 800, # https://www.researchgate.net/post/How_much_cost_one_tone_of_sulfuric_acid_96 gives 300 USD per ton
    "coal": 287, # https://tradingeconomics.com/commodity/coal gives 110 USD per ton
    "saskatoons": 23640, # https://www.saskatoonberry.com/fs.html gives 130 CAD for 10 kg
    "lumber": 531, # https://www.nasdaq.com/markets/lumber.aspx gives USD 437 per "board foot" (apparently a unit of volume)
    "engines": 800 * 40, # ??? (both price and units/pallet are guesses)
    "magnets": 45720, # https://amfmagnets.com/ferrite-block-magnet-75mm-x-50mm-x-20mm.html
    "limestone": 71, # https://minerals.usgs.gov/minerals/pubs/commodity/stone_crushed/cstonmyb03.pdf gives "$27.25 per ton"
    "oil": 1091, # https://markets.businessinsider.com/commodities/oil-price gives $418.38 per ton
    "shirts": 3* 2160, # https://www.theadairgroup.com/blog/2012/01/30/bulk-wholesale-t-shirts/ gives 2160 shirts per pallet, https://www.jonestshirts.com/tshirts gives ~$3 per shirt 
    "mackerel": 4565, # https://www.undercurrentnews.com/2016/12/06/mackerel-prices-expected-to-ease-early-in-2017/ gives $1.75 ??? this is way out of proportion with the other fish
    "grass": 420000, # https://abc30.com/news/pallet-of-pot-discovered-at-shipping-company-in-kettleman-city/726446/ gives $329000 USD, which is too close to 420k CAD to pass up
    "lobster": 40000, # http://www.myseafood.com/images/products/Specs-Info/PDF/Lobster/5kg%20Live%20Lobster%20(Market%20A)%20Specification%20Table.pdf gives 400kg on typical pallet, http://www.yankeelobstercompany.com/market-prices/ gives $10 / pounds
    "leather": 5739, # http://www.nationalbeefleather.com/media/1101/gradec_specsheet.pdf gives $2200 USD per what looks like a half-pallet
    "birdseed": 866, # http://www.mws-d.com/ProductDetail.aspx?productid=916939 gives $8.30 USD per bag, 80 per pallet
    "canola": 1168, # http://www.centrafoods.com/blog/bid/324076/How-Much-Bulk-Olive-Oil-is-on-a-Pallet gives 2000 lbs per pallet, https://www.canolacouncil.org/markets-stats/statistics/current-canola-oil,-meal,-and-seed-prices/ gives $988 USD per ton
    "loonies": 125000, # https://www.coincommunity.com/forum/topic.asp?TOPIC_ID=275259&whichpage=1 gives "$100-150K loonies/pallet"
    "grain": 257, # http://www.brewingwithbriess.com/Orderinfo/Packaging_Options_Dry.htm gives 900 kg per pallet, https://www.indexmundi.com/commodities/?commodity=wheat gives $219 USD per ton
    "pike": 47050, # http://www.walleyedirect.com/product/northern-pike-11.html give $90 USD per 11 lbs
    "videogames": 1220 * 60, # https://bstocksupply.com/auction/auction/view/id/78254/ gives 1220 games per pallet, video games are $60 each
    "antennas": 5254, # https://hilineelectronics.com/wholesale-pallet-of-185-antennas-retail-5254-29-untested-customer-returns-ge-rca-onn/ gives this as retail price
    "wigs": 42050 # this one's completely a guess (???)

}

# ========== end data ==========

import random
random.seed(12345)

outfile = open('data/commodity_data.mdcl', 'w')
walkthrough_file = open('LetsExploreGeography-dist/walkthrough.html', 'w')
walkthrough_file.write(open('walkthrough_static_part.html').read())

city_slugs = dict(map(lambda s: s.strip().split("\t"), open('data/important_cities.txt', 'r').readlines()))
important_cities = set(city_slugs.keys())
cities_with_commodities = set(city_commodities.keys())
commodities = set(city_commodities.values())
commodities_with_prices = set(commodity_pallet_prices.keys())

commodity_buy_locations = {}
commodity_sell_locations = {}

for city in important_cities - cities_with_commodities:
    print("Warning: city", city, 'has no commodities')

for commodity in commodities - commodities_with_prices:
    print("Warning: commodity", commodity, 'has no price')
    commodity_pallet_prices[commodity] = 0

for city in cities_with_commodities - important_cities:
    print("Warning: unknown city",city)

for commodity in commodities_with_prices - commodities:
    print("Warning: price for unknown commodity", commodity)

for commodity in commodities:
    price = commodity_pallet_prices[commodity]
    print("commodity [" + commodity + ", " + str(price) + "]", file=outfile)

print(file=outfile)

commodities = sorted(commodities)
important_cities = sorted(important_cities)

def fudge_sell_price(commodity):
    price = commodity_pallet_prices[commodity]
    if commodity == "loonies":
        # loonies do not typically change in value
        return price
    multiplier = random.uniform(0.9, 1.2)
    if multiplier < 1:
        multiplier = 1 - (1 - multiplier) * 2
    return int(price * multiplier)

for city in important_cities:
    print("when current_city = city_" + city + " {", file=outfile)
    if city in cities_with_commodities:
        selling = city_commodities[city]
        buylist = random.sample(commodities, 4)
        if selling in buylist:
            buylist.remove(selling)
        else:
            buylist = buylist[:3]
        buys1, buys2, buys3 = buylist
    else:
        selling, buys1, buys2, buys3 = random.sample(commodities, 4)
    print("    current_city_sells = commodity_" + selling, file=outfile)
    print("    current_city_buys1 = commodity_" + buys1, file=outfile)
    print("    current_city_buys2 = commodity_" + buys2, file=outfile)
    print("    current_city_buys3 = commodity_" + buys3, file=outfile)
    if selling not in commodity_buy_locations:
        commodity_buy_locations[selling] = []
    commodity_buy_locations[selling].append(city)
    for buying in (buys1, buys2, buys3):
        buy_price = fudge_sell_price(buying)
        if buying not in commodity_sell_locations:
            commodity_sell_locations[buying] = []
        commodity_sell_locations[buying].append((city, buy_price))
        print("    commodity_" + buying + "_price =", buy_price, file=outfile)
    print("}", file=outfile)

print(file=outfile)

print("when will_liquidate_commodities {", file=outfile)
print("    next_cash = current_cash", file=outfile)
for commodity in commodities:
    print("        + ( current_truck_" + commodity, "*", commodity_pallet_prices[commodity], ")", file=outfile)

for commodity in commodities:
    print("    next_truck_" + commodity, "= 0", file=outfile)
print("}", file=outfile)

print("inventory_screen.content: when not(show_hud) \"[NO SIGNAL]\" when current_truck_all_pallets = 0 and not(has_either_artifact) \"Your truck is currently empty.\" otherwise \"Your truck currently contains the following:\n", file=outfile)
for commodity in commodities:
    print("${", file=outfile)
    print("   when current_truck_" + commodity + " = 0 \"\"", file=outfile)
    print("   when current_truck_" + commodity + " = 1 \"- A pallet of "+commodity+", which you bought for \$" + str(commodity_pallet_prices[commodity])+"\n\"", file=outfile)
    print("   otherwise \"- ${current_truck_"+commodity+"} pallets of "+commodity+", which you bought for \$" + str(commodity_pallet_prices[commodity])+" each\n\"", file=outfile)
    print("}", end="", file=outfile)
print("${when has_wooden_nickel \"- A wooden nickel from the Fredericton tourism office\n\" otherwise \"\"}", end="", file=outfile)
print("${when has_spiderman \"- That Spider-Man comic you've been looking for\" otherwise \"\"}", end="", file=outfile)
print("\"", file=outfile)

print("<table border=1>", file=walkthrough_file)
for commodity in sorted(commodities):
    print("<tr><td class=\"commodity\">"+commodity+"</td><td>", file=walkthrough_file)
    if commodity in commodity_buy_locations:
        print("Buy in", " or ".join(map(lambda n: city_slugs[n], commodity_buy_locations[commodity])), "for $" + str(commodity_pallet_prices[commodity]), file=walkthrough_file)
    print("</td><td>", file=walkthrough_file)
    if commodity in commodity_sell_locations:
        print("<br>\n".join(map(lambda x: "Sell in " + city_slugs[x[0]] + " for $" + str(x[1]), commodity_sell_locations[commodity])), file=walkthrough_file)
    print("</td></tr>", file=walkthrough_file)
print("</table>", file=walkthrough_file)
