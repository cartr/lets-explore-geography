#!/usr/bin/env python3

import csv

outfile = open('data/tourism_data.mdcl', 'w')
for city_slug, menu, desc, past, desc_revised in csv.reader(open("tourism.csv", newline="")):
    if "City" in city_slug:
        continue # skip header row
    if not menu.strip():
        continue # skip cities with no tourism
    if not past.strip():
        print("WARNING: city", city_slug, "has a menu option but no past-tense, skipping")
        continue
    if desc_revised.strip():
        desc = desc_revised.strip()
    elif desc.strip():
        desc = desc.strip()
        print("WARNING: city", city_slug, "has no revised tourism description, using unrevised text")
    else:
        print("WARNING: city", city_slug, "has a menu option but no description, skipping")
        continue
    desc = desc.replace("\"", "\\\"") # escape quotes in description text
    desc = desc.replace("{", "${ when city_"+city_slug+"_did_tourism = 0 \"")
    desc = desc.replace("|", "\" otherwise \"")
    desc = desc.replace("}", "\"}")
    print("city_tourism [city_"+city_slug+", \""+menu.strip().replace("\"", "\\\"")+"\", \""+desc+"\", \""+past.strip().replace("\"", "\\\"")+"\"]", file=outfile)
